﻿using System.Collections.Generic;
using Mystic8BallBot;
using NUnit.Framework;

namespace Mystic8BallBotTests
{
    [TestFixture]
    public class PredictorTests
    {
        [Test]
        public void PredictorReturnsNotEmptyResult()
        {
            // Execute
            var result = new Predictor().GetPrediction();

            // Validate
            Assert.IsNotEmpty(result);
        }

        [Test]
        public void PredictorReturnsDifferentResultsFor100Runs()
        {
            // Prepare
            var answers = new List<string>();
            var predictor = new Predictor();

            // Execute
            for (var i = 0; i < 100; i++)
            {
                var result = predictor.GetPrediction();
                if (!answers.Contains(result))
                {
                    answers.Add(result);
                }
            }

            // Validate
            Assert.AreNotEqual(1, answers.Count);
        }
    }
}
