﻿using System;

namespace Mystic8BallBot
{
    public class Predictor
    {
        private readonly Random random;

        public Predictor()
        {
            this.random = new Random(DateTime.UtcNow.Millisecond);
        }

        private static String[] answers = new[]
        {
            Answers.ItIsCertain,
            Answers.ItIsDecidedlySo,
            Answers.WithoutADoubt,
            Answers.YesDefinitely,
            Answers.YouMayRelyOnIt,
            Answers.AsISeeItYes,
            Answers.MostLikely,
            Answers.OutlookGood,
            Answers.SignsPointToYes,
            Answers.Yes,
            Answers.ReplyHazyTryAgain,
            Answers.AskAgainLater,
            Answers.BetterNotTellYouNow,
            Answers.CannotPredictNow,
            Answers.ConcentrateAndAskAgain,
            Answers.DontCountOnIt,
            Answers.MyReplyIsNo,
            Answers.MySourcesSayNo,
            Answers.OutlookNotSoGood,
            Answers.VeryDoubtful
        };

        public string GetPrediction()
        {
            var answerNumber = this.random.Next(0, answers.Length - 1);
            return answers[answerNumber];
        }
    }
}