﻿using System;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using Microsoft.Bot.Connector;

namespace Mystic8BallBot.Controllers
{
    [BotAuthentication]
    public class MessagesController : ApiController
    {
        private readonly Predictor predictor = new Predictor();

        /// <summary>
        /// POST: api/Messages
        /// Receive a activity from a user and reply to it
        /// </summary>
        public async Task<HttpResponseMessage> Post([FromBody]Activity activity)
        {
            var connector = new ConnectorClient(new Uri(activity.ServiceUrl));

            if (activity.Type == ActivityTypes.Message)
            {
                if (activity.Text != null && activity.Text.EndsWith("?"))
                {
                    var reply = activity.CreateReply(this.predictor.GetPrediction());
                    await connector.Conversations.ReplyToActivityAsync(reply);
                }
                else
                {
                    var reply = activity.CreateReply("It isn't a question. Please ask me a question.");
                    await connector.Conversations.ReplyToActivityAsync(reply);
                }
            }

            return Request.CreateResponse(HttpStatusCode.OK);
        }
    }
}